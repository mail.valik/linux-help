# System

## Storage

### Check occupied space by folders
Shows in human readable format
```
sudo du -sh *
```

Shows in kilos and sorted
```
du -sk * | sort -n
```

Shows with total and sorted
```
du -sch * .[!.]* | sort -rh
```

Shows last 5 with total and sorted
```
sudo du -hs * | sort -rh | head -5
```


## Files and folders

### Search for text in files recursively
```
grep -rnw /some/folder -e 'site.ru'
```

### Replace text in files recursively
```
grep -rnwl /some/folder -e 'site.ru' | xargs sed -i 's/site.ru/newsite.ru/g'
```

## Logs and journal

### Services logs
```
journalctl -f -u inlets
journalctl -f -u caddy
```

### Check open files by a process
- grab process id `ps aux | grep process_name`
- find process id in result output
- count open files `sudo ls -l /proc/[proc_id]/fd | wc -l`
- show open files `sudo ls -l /proc/[proc_id]/fd`
- TODO: what is diff?
- count open files `sudo lsof -p [proc_id] | wc -l`
- show open files `sudo lsof -p [proc_id]`
