# Linux Help

## Commands
- [Network]
- [System]

[Network]: https://gitlab.com/mail.valik/linux-help/-/blob/master/network.md
[System]: https://gitlab.com/mail.valik/linux-help/-/blob/master/system.md

## Articles

### Core
- Interrupts
  - https://0xax.gitbooks.io/linux-insides/content/Interrupts/
  - https://linux-kernel-labs.github.io/refs/heads/master/lectures/interrupts.html#